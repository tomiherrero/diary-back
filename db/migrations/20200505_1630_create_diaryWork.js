exports.up = knex => knex.schema
    .createTable('diaryWork', table => {
        table.string('id');
        table.string('homework');
        table.string('date');
        table.string('iso2', 2); /*que significa iso2*/
        table.boolean('deleted');
        table.timestamp('createdAt');
        table.timestamp('updatedAt');
        table.timestamp('deletedAt');
        table.integer('__v');
    });

exports.down = knex => knex.schema
    .dropTable('diaryWork');
