const diaryWork = require('./diaryWork');
const diaryFaculty = require('./diaryFaculty');


module.exports = {
    ...diaryWork,
    ...diaryFaculty
};
