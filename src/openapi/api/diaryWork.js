module.exports = {
    '/api/diaryWork': {
        get: {
            security: [],
            summary: 'Work to do list',
            parameters: [],
            responses: {
                200: {
                    description: 'Work to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/DiaryWork'}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post: {
            security: [],
            requestBody:{ 
                description: 'Optional description in *Markdown*', 
                required: true, 
                content: {'application/json': {schema: {$ref: '#/components/schemas/DiaryWork'}}}
            },
            responses: {
                200:{
                    description: 'Work to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/diaryWork/{id}': {
        get: {
            security: [], 
            parameters: [
                {
                    in: 'path',
                    name: 'id', 
                    required: true, 
                    schema: {
                        type: 'string',
                        format: 'uuid'
                    }, 
                    description: 'requested work'
                }
            ],
            responses: {
                200: {
                    description: 'Work to do list', 
                    content: {'application/json': {schema: {$ref: '#/components/schemas/DiaryWork'}}}
                },
                default: {
                    description: 'Error', 
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }, 
        put:{
            security: [],
            parameters: [
                {
                    in: 'path',
                    name: 'id',
                    required: true,
                    schema: {
                        type: 'string',
                        format: 'uuid'
                    }, 
                    description: 'work to do list'
                }
            ],
            requestBody: {
                description: 'Optional description in *Markdown*', 
                required: true, 
                content: {'application/json': {schema: {$ref: '#/components/schemas/DiaryWork'}}}
            },
            responses: { 
                200: {
                    description: 'Work to do list', 
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {} 
                            }
                        }
                    }
                },
                default: {
                    description: 'Error', 
                    content: {'application/json': {schema : {$ref: '#/components/schemas/Error'}}}
                }

            }
        },
        delete: {
            security: [],
            parameters: [
                {
                    in: 'path',
                    name: 'id',
                    required: true,
                    description: 'Work to do list',
                    schema: {
                        type: 'string',
                        format: 'uuid'
                    }
                }
            ],
            responses: {
                200:{
                    description: 'Work to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object', 
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error', 
                    content: {'application/json': {schema: {$ref:'#/components/schemas/Error'}}}
                }
            }
        }
    }
};