module.exports = {
    '/api/diaryFaculty': {
        get: {
            security: [],
            summary: 'Faculty to do list',
            parameters: [],
            responses: {
                200: {
                    description: 'Faculty to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'array',
                                items: {$ref: '#/components/schemas/DiaryFaculty'}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        post: {
            security: [], 
            requestBody: {
                description: 'Optional description in *Markdown*',
                required: true, 
                content: {'application/json': {schema:{$ref:'#/components/schemas/DiaryFaculty'}}}
            },
            responses: {
                200: {
                    description: 'Faculty to do list',
                    content:{
                        'application/json': {
                            schema: {
                                type: 'object', 
                                properties: {}
                            }
                        }
                    }
                }, 
                default: {
                    description: 'Error',
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    },
    '/api/diaryFaculty/{id}': {
        get: {
            security: [], 
            parameters: [
                {
                    in: 'path',
                    name: 'id', 
                    required: true,
                    schema: {
                        type: 'string', 
                        format: 'uuid'
                    }, 
                    description: 'requested matter'
                }
            ],
            responses: {
                200: {
                    description: 'Faculty to do list', 
                    content: {'application/json': {schema:{$ref:'#/components/schemas/DiaryFaculty'}}}
                }, 
                default: {
                    description: 'Error', 
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        put: {
            security: [], 
            parameters: [
                {
                    in: 'path', 
                    name: 'id', 
                    schema: {
                        type: 'string', 
                        format: 'uuid'
                    }, 
                    required: true, 
                    description: 'request matter'
                }
            ],
            requestBody: {
                description: 'Optional description in *Markdown*', 
                required: true, 
                content: {'application/json': {schema: {$ref: '#/components/schemas/DiaryFaculty'}}}
            },
            responses: {
                200: {
                    description: 'Work to do list',
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error', 
                    content: {'application/json': {schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        },
        delete: {
            security: [],
            parameters: [
                {
                    in: 'path',
                    name: 'id',
                    required: true,                    
                    description: 'Faculty to do list',
                    schema: {
                        type: 'string',
                        format: 'uuid'
                    }
                }
            ],
            responses: {
                200: {
                    description: 'Faculty to do list', 
                    content: {
                        'application/json': {
                            schema: {
                                type: 'object',
                                properties: {}
                            }
                        }
                    }
                },
                default: {
                    description: 'Error',
                    content: {'application/json':{schema: {$ref: '#/components/schemas/Error'}}}
                }
            }
        }
    }
};