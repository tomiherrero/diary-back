const ModelCreate = include('helpers/modelCreate');
const tableName = 'diaryWork';
const name = "DiaryWork";
const selectableProps =[ 
    'id', 
    'homework',
    'date',
    'iso2',
    'deleted', 
];


class diaryWork extends ModelCreate {
    constructor(props){
        super({ 
            ...props,
            tableName,
            name,
            selectableProps
        })
    }
}

module.exports = knex => new diaryWork({knex})
