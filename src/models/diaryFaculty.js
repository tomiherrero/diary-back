const ModelCreate = include('helpers/modelCreate');
const tableName = 'diaryFaculty';
const name = "DiaryFaculty";


const selectableProps = [
    'id',
    'homework',
    'matter',
    'date',
    'iso2',
    'deleted' 

]



class diaryFaculty extends ModelCreate {
    constructor(props){
        super({
            ...props,
            tableName,
            name,
            selectableProps
        })
    }
}

module.exports = knex => new diaryFaculty({knex})
