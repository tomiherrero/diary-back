const {DiaryFacultyController} = include('controllers')

module.exports = router => {

    router.route('/')
        .get(DiaryFacultyController.fetch)
        .post(DiaryFacultyController.create)
    router.route('/:id')
        .get(DiaryFacultyController.fetchOne)
        .put(DiaryFacultyController.save)
        .delete(DiaryFacultyController.delete);


    return router;
}