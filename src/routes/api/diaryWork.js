const {DiaryWorkController} = include('controllers'); 


module.exports = router => {
    
    router.route('/')
        .get(DiaryWorkController.fetch)
        .post(DiaryWorkController.create);
    router.route('/:id')
        .get(DiaryWorkController.fetchOne)
        .put(DiaryWorkController.save)
        .delete(DiaryWorkController.delete);
    return router;
}