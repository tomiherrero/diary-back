const {DiaryFaculty} = include('models');


class DiaryFacultyController {
    static async fetch(req, res, next){
        try{
            const diarysF = await DiaryFaculty.find({...req.query, deleted: null });
            res.send(diarysF);
        }catch(error){
            next(error);
        }
    }
    static async fetchOne(req, res, next){
        try{
            const diaryF = await DiaryFaculty.findOne({id: req.params.id});
            res.send(diaryF);
        }catch(error){
            next(error)
        }
    }
    static async create(req, res, next){
        try{ 
            const result = await DiaryFaculty.insertOne(req.body);
            res.send({status: 'success', result})

        } catch(error){ 
            next(error)
        }   
    }
    static async save(req, res, next){
        try{
            const result = await DiaryFaculty.updateOne({id: req.params.id}, req.body)
            res.send(result)
        }catch(error){
            next(error);
        }
    }
    static async delete(req, res, next) {
        try{
            const result = await DiaryFaculty.deletedOne(req.params.id);
            res.send({succes: true, result});
        }catch(error){
            next(error);
        }
    }
}

module.exports = DiaryFacultyController;