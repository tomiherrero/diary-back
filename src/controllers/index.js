module.exports = {
    DiaryWorkController: require('./diaryWork'),
    DiaryFacultyController: require('./diaryFaculty'),
    StatusController: require('./status')
};
