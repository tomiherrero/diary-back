const {DiaryWork} = include('models');


class DiaryWorkController {
    static async fetch(req, res, next){
        try{
            const diarysW = await DiaryWork.find({...req.query, deleted: null});
            res.send(diarysW);
        }catch (error){
            next(error)
        }
    }

    static async fetchOne(req, res, next) {
        try{
            const diaryW = await DiaryWork.findOne({id: req.params.id});
            res.send(diaryW);
        }catch (error){
            next(error)
        }
    }
    
    static async create(req, res, next){
        try{   
            const result = await DiaryWork.insertOne(req.body);
            res.send({satus: 'success', result});
            }catch(error){
            next(error)
        }
    }

    static async save(req, res, next){
        try{
            const result = await DiaryWork.updateOne({id: req.params.id}, req.body);
            res.send(result);
        }catch(error){
            next(error)
        }
    }
    
    static async delete(req, res, next){
        try{
            const result = await DiaryWork.deletedOne(req.params.id);
            res.send({succes: true, result})
        }catch(error){
            next(error)
        }
    }
}

module.exports = DiaryWorkController;